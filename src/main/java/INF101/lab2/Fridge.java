package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    final int maxItems = 20;
    ArrayList<FridgeItem> itemsFridge = new ArrayList<FridgeItem>(); 

    @Override
    public int nItemsInFridge() {
        return itemsFridge.size() ;
    }

    @Override
    public int totalSize() {
        return maxItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemsFridge.size() < maxItems){
            itemsFridge.add(item);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemsFridge.contains(item)){
            itemsFridge.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        itemsFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        for (int i = 0; i < itemsFridge.size(); i++){
            if ((itemsFridge.get(i)).hasExpired()) {
                expiredFood.add(itemsFridge.get(i));
            }    
        }
                
        for (int i = 0; i < expiredFood.size(); i++){
            if (itemsFridge.contains(expiredFood.get(i))){
                itemsFridge.remove(expiredFood.get(i));
            }
        }
        return expiredFood;
    }
}